package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.util.TerminalUtil;
import ru.korkmasov.tsc.util.ValidationUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public final class TaskByIndexSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by index";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber();
        //if (!checkIndex(index, serviceLocator.getTaskEndpoint().countTask(serviceLocator.getSession())))
        //    throw new IndexIncorrectException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        //serviceLocator.getTaskEndpoint().setTaskStatusByIndex(serviceLocator.getSession(), index, Status.valueOf(TerminalUtil.nextLine()));
    }

}
