package ru.korkmasov.tsc.command.task;

import ru.korkmasov.tsc.enumerated.Status;
import ru.korkmasov.tsc.exception.entity.TaskNotFoundException;
import ru.korkmasov.tsc.model.Task;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;

public class TaskByIdSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by id";
    }

    @Override
    public void execute() {
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        //if (!serviceLocator.getTaskEndpoint().existsTaskById(serviceLocator.getSession(), id))
        //    throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        //serviceLocator.getTaskEndpoint().setTaskStatusById(serviceLocator.getSession(), id, Status.fromValue(TerminalUtil.nextLine()));

    }

}
