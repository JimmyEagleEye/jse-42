package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.empty.EmptyNameException;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.exception.system.IndexIncorrectException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

import static ru.korkmasov.tsc.util.TerminalUtil.incorrectValue;

public class ProjectByIndexUpdateCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "project-update-by-index";
    }

    @Override
    public @Nullable String description() {
        return "Update project by index";
    }

    @Override
    public void execute() {
        System.out.println("Enter index");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByIndex(serviceLocator.getSession(), index);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Project projectUpdated = serviceLocator.getProjectEndpoint()
                .updateProjectByIndex(serviceLocator.getSession(), index, name, description);
        if (projectUpdated == null) incorrectValue();
    }

}
