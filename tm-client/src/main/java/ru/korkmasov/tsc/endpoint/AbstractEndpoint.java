package ru.korkmasov.tsc.endpoint;

import ru.korkmasov.tsc.api.service.ServiceLocator;

public class AbstractEndpoint {

    protected final ServiceLocator serviceLocator;

    AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
