package ru.korkmasov.tsc.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "abstractBusinessEntity", propOrder = {
        "userId"
})
@XmlSeeAlso({
        Session.class
})
public abstract class AbstractBusinessEntity
        extends AbstractEntity {

    protected String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String value) {
        this.userId = value;
    }

}
