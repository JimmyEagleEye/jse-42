package ru.korkmasov.tsc.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.endpoint.IAdminEndpoint;
import ru.korkmasov.tsc.api.service.ServiceLocator;
import ru.korkmasov.tsc.dto.SessionDTO;
import ru.korkmasov.tsc.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    public AdminEndpoint() {
        super(null);
    }

    public AdminEndpoint(
            @NotNull final ServiceLocator serviceLocator
    ) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void loadBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

    @Override
    @WebMethod
    public void loadJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

    @Override
    @WebMethod
    public void saveBackup(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

    @Override
    @WebMethod
    public void saveJson(
            @WebParam(name = "session", partName = "session") @NotNull final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
    }

}
