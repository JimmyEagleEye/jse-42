package ru.korkmasov.tsc.exception.user;

import ru.korkmasov.tsc.exception.AbstractException;

public class NotLoggedInException extends AbstractException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}
