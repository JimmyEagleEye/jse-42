package ru.korkmasov.tsc.constant;

public interface TableConst {

    String SESSION_TABLE = "session";

    String USER_TABLE = "\"user\"";

    String TASK_TABLE = "task";

    String PROJECT_TABLE = "project";
}
