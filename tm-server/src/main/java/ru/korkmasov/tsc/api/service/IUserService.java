package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.UserDTO;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public interface IUserService extends IService<UserDTO> {

    @Nullable
    UserDTO findById(@Nullable String id);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    //@NotNull
    //UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    UserDTO lockUserByLogin(String login);

    UserDTO unlockUserByLogin(String login);

    @NotNull
    UserDTO add(@NotNull String login, @NotNull String password);

    UserDTO add(final String login, final String password, final String email);

    @NotNull
    UserDTO setPassword(@NotNull String id, @NotNull String password);

    @Nullable
    UserDTO updateUser(
            @NotNull String id,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String middleName
    );

    //boolean existsByLogin(@NotNull String login);
}
