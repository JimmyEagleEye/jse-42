package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.dto.SessionDTO;
import ru.korkmasov.tsc.dto.UserDTO;
import ru.korkmasov.tsc.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<SessionDTO> {

    @Nullable
    SessionDTO open(@Nullable String login, @Nullable String password);

    UserDTO checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionDTO session, Role role);

    void validate(@Nullable SessionDTO session);

    @Nullable
    SessionDTO sign(@Nullable SessionDTO session);

    void close(@Nullable SessionDTO session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionDTO> findAllByUserId(@Nullable String userId);

}
