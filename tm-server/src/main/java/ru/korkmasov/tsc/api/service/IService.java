package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IRepository;
import ru.korkmasov.tsc.dto.AbstractEntityDTO;

public interface IService<E extends AbstractEntityDTO> extends IRepository<E> {
}
