package ru.korkmasov.tsc.api.entity;

import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStart {

    @Nullable
    Date getStartDate();

    void setStartDate(@Nullable Date dateStart);

}
