package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.TaskDTO;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import java.util.Collection;

public interface ITaskService extends IService<TaskDTO> {

    //List<TaskDTO> findAll(@NotNull String userId, Comparator<TaskDTO> comparator);

    @NotNull
    TaskDTO add(@NotNull String userId, @Nullable String name, @Nullable String description);

    void clear(@NotNull String userId);

    void clear();

    //void removeOneById(@NotNull String userId, @Nullable String id);

    //@Nullable
    //TaskDTO findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    //void removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull TaskDTO finishTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    TaskDTO findOneByIndex(@NotNull String userId, @Nullable Integer index);

    TaskDTO finishTaskById(@NotNull String userId, @Nullable String id);

    @Nullable
    TaskDTO finishTaskByName(@NotNull String userId, @Nullable String name);

    //@Nullable
    //TaskDTO changeTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    //@Nullable
    //TaskDTO changeTaskStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    //@Nullable
    //TaskDTO changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    //String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    //int size(@NotNull String userId);

    //boolean existsByName(@NotNull String userId, @NotNull String name);

    //boolean existsById(@NotNull String userId, @NotNull String id);

    //void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    //void setTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    //void setTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    @NotNull TaskDTO startTaskById(@NotNull String userId, @Nullable String id);

    @NotNull TaskDTO startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull TaskDTO startTaskByName(@NotNull String userId, @Nullable String name);

    @NotNull TaskDTO updateTaskById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull TaskDTO updateTaskByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    //void updateTaskByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

    @NotNull
    @SneakyThrows
    List<TaskDTO> findAll(@NotNull String userId);

    @SneakyThrows
    void addAll(@NotNull String userId, @Nullable Collection<TaskDTO> collection);

    @Nullable
    @SneakyThrows
    TaskDTO add(@NotNull String userId, @Nullable TaskDTO entity);

    @Nullable
    @SneakyThrows
    TaskDTO findById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void removeById(@NotNull String userId, @Nullable String id);

    @SneakyThrows
    void remove(@NotNull String userId, @Nullable TaskDTO entity);

}
