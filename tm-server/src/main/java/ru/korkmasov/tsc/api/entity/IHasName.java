package ru.korkmasov.tsc.api.entity;

import org.jetbrains.annotations.Nullable;

public interface IHasName {

    @Nullable
    String getName();

    void setName(@Nullable String name);

}
