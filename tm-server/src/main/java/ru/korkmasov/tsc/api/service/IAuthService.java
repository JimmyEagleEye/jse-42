package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.UserDTO;
import ru.korkmasov.tsc.enumerated.Role;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

    void checkRoles(@Nullable Role... roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, String password);

    @NotNull UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
