package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.api.repository.IOwnerRepository;
import ru.korkmasov.tsc.dto.AbstractOwnerDTO;

public interface IOwnerService<E extends AbstractOwnerDTO> extends IService<E>, IOwnerRepository<E> {
}
