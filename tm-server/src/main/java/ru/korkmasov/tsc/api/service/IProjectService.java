package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.korkmasov.tsc.dto.ProjectDTO;

import java.util.Collection;
import java.util.List;

public interface IProjectService extends IService<ProjectDTO> {

    //@Nullable
    //ProjectDTO findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO add(@NotNull String userId, @NotNull String name, @NotNull String description);

    //@NotNull
    //ProjectDTO removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO findOneByName(@NotNull String userId, @NotNull String name);

    @NotNull
    void removeOneByName(@NotNull String userId, @NotNull String name);

    void removeProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO findOneByIndex(String userId, Integer index);

    @NotNull
    ProjectDTO updateProjectByIndex(@NotNull String userId, @NotNull Integer index, @NotNull String name, @Nullable String description);

    @NotNull
    ProjectDTO updateProjectById(String userId, String id, String name, String description);

    //@NotNull
    //ProjectDTO updateProjectByName(@NotNull String userId, @NotNull String name, @NotNull String nameNew, @Nullable String description);

    @NotNull
    ProjectDTO startProjectById(@NotNull String userId, @NotNull String id);

    @NotNull
    ProjectDTO startProjectByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectDTO startProjectByName(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectDTO finishProjectById(@NotNull String userId, @NotNull String id);

    @Nullable
    ProjectDTO finishProjectByIndex(@NotNull String userId, @Nullable Integer index);

    @Nullable
    ProjectDTO finishProjectByName(@NotNull String userId, @Nullable String name);

    //@Nullable
    //ProjectDTO changeProjectStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    //@Nullable
    //ProjectDTO changeProjectStatusByName(@NotNull String userId, @Nullable String name, @Nullable Status status);

    //@Nullable
    //ProjectDTO changeProjectStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    //boolean existsByName(String userId, String name);

    //void setProjectStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    //void setProjectStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    //void setProjectStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    void addAll(@NotNull String userId, @Nullable Collection<ProjectDTO> collection);

    @Nullable
    ProjectDTO add(@NotNull String userId, @Nullable ProjectDTO entity);

    @Nullable
    ProjectDTO findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable ProjectDTO entity);

}
