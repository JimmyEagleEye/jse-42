package ru.korkmasov.tsc.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractOwner extends AbstractEntity {

    @Nullable
    @ManyToOne
    protected User user;

}


