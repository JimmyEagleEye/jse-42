package ru.korkmasov.tsc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter

public class DomainDTO implements Serializable {

    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    @Nullable
    private List<ProjectDTO> projects;

    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    @Nullable
    private List<TaskDTO> tasks;

    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    @Nullable
    private List<UserDTO> users;

    public void setUsers(@NotNull List<UserDTO> users) {
        this.users = users;
    }

    public void setProjects(@NotNull List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull List<TaskDTO> tasks) {
        this.tasks = tasks;
    }
}
