package ru.korkmasov.tsc.dto;

import ru.korkmasov.tsc.enumerated.Role;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;

@Setter
@Getter
@Entity
@Table(name = "tm_user")
@NoArgsConstructor
public class UserDTO extends AbstractEntityDTO {

    @Column
    @NotNull
    private String login;

    @NotNull
    @Column(name = "password_hash")
    private String passwordHash;

    @Column
    @Nullable
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;


    @Nullable
    private boolean locked = false;

    public boolean isLocked() {
        return locked;
    }

}
